﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform player;
    public Vector3 cameraOffset;

    private void Update()
    {
        transform.position = player.position+cameraOffset;
    }
}
